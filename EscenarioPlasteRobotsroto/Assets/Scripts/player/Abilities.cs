﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Abilities : MonoBehaviour 
{
	//tipos de robot 
	private bool typeDef=false;
	private bool typeAtk=true;
	private bool typeVel=false;
	private bool abilitiePasive=false;
	private bool pasiveShield=true;
	private bool shieldCouldown=false;
	private bool DashCouldown=false;
	private bool rechargeCouldown=false;
	private bool jumpCouldown=false;
	private bool invisibleCouldown=false;
	private bool explosionCouldown=false;
	public Transform target;
	public GameObject shield;
	public GameObject shieldPasive;
	public GameObject damageArea;
	public Transform spawnR;
	private float timeCoulDown=5f;
	private float timeA=6;
	private float timeB=6;
	private float timeC=6;
	private float timeD=6;
	private float timeE=6;
	private float timeF=6;
	private float timeShield;
	private float timeDash;
	private float timeJump;
	private float timeinvisible;
	private float timeRecharge;
	private float timeExplosion;
	private int PasiveDamageBullet;//daño de la ultima bala de la habilidad pasiva
	private float dash;
	// Use this for initialization
	void Start () 
	{
		PasiveDamageBullet = GameManager.init.PasiveDamageBullet;
		dash = GameManager.init.dashImpulse;
		timeShield=GameManager.init.timeShield;
		timeDash=GameManager.init.timeDash;
		timeJump=GameManager.init.timeJump;
		timeinvisible=GameManager.init.timeinvisible;
		timeRecharge=GameManager.init.timeRecharge;
		timeExplosion=GameManager.init.timeExplosion;
	}
	
	// Update is called once per frame
	void Update () 
	{


		active ();
		pasive ();
		CoulDown ();

	}

	private void pasive()
	{
		//habilidades pasivas
		if (typeDef)
		{
			/**
	 		* escudo durante un tiempo 
			 **/
			abilitiePasiveShildHP ();
		}
		else if(typeAtk)
		{
			/**
	 		* ultima bala mete mas daño 
			 **/
			ultimateBullet ();
		}
		else if(typeVel)
		{
			/**
	 		* mayor velocidad de disparo
			 **/
			SpeedBulletBurst ();
		}
	}

	private void active()
	{
		Def ();
		Atk ();
		Vel ();
	}

	private void Def()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1) && typeDef && !shieldCouldown)
		{
			shieldCouldown = true;
			Shield (spawnR);

		}

		if (Input.GetKeyDown(KeyCode.Alpha2) && typeDef && !explosionCouldown)
		{
			//habilidad daño en area
			GameObject clon = (GameObject)Instantiate (damageArea, gameObject.transform.position, Quaternion.identity,gameObject.transform);
			clon.transform.localScale = new Vector3 (10f, 10f, 10f);
			Destroy (clon, 1);
			Invoke ("NormalVelocity", GameManager.init.slowDawntime);
		}
	}

	private void Atk()
	{
		
		if (Input.GetKeyDown(KeyCode.Alpha1) && typeAtk && !DashCouldown)
		{
			//habilidad activa Dash a donde mira
				DashCouldown=true;
				Dash();
			
		}

		if (Input.GetKeyDown(KeyCode.Alpha2) && typeAtk && !rechargeCouldown)
		{
			//habilidad activa recargar cargador
				rechargeCouldown=true;
				gameObject.GetComponent<Attack>().bullet=GameManager.init.bullet;
		}
	}

	private void Vel()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1) && typeVel && !jumpCouldown)
		{
			//habilidad activa saltar muros
			jumpCouldown=true;
			gameObject.GetComponent<MovimientoPersonaje>().moveDirection.y=GameManager.init.jump+GameManager.init.JumpPasiveAbilitie;
				
		}

		else if (Input.GetKeyDown(KeyCode.Alpha2) && typeVel && !invisibleCouldown)
		{
			//gameObject.GetComponents<MeshRenderer>().
				//invisible/trasparente 
				invisibleCouldown=true;
				Color color = GetComponent<Renderer>().material.color;
				GetComponent<Renderer>().material.color = new Color(color.r, color.g,color.b,0.15f);
				Invoke("normal",timeinvisible);

		}
	}

	private void Shield (Transform arm)
	{
		
		GameObject clon = (GameObject)Instantiate (shield, arm.transform.position, Quaternion.identity,arm.transform);//emparentar
		clon.transform.LookAt (target);
		Destroy (clon, timeShield);
		timeA += Time.deltaTime;
		//instancia un escudo inpenetrable que se puede arrastrar 
	}

	private void ultimateBullet()
	{
		if (gameObject.GetComponent<Attack> ().bullet == 1) 
		{
			GameManager.init.damageBullet += PasiveDamageBullet;
			abilitiePasive = true;
		} 
		else 
		{
			if (abilitiePasive) 
			{
				GameManager.init.damageBullet -= PasiveDamageBullet;
				abilitiePasive = false;
			}
		}
	}

	public void Dash()
	{
		gameObject.GetComponent<CharacterController>().Move(gameObject.GetComponent<CharacterController>().transform.forward*dash);
	}

	public void SpeedBulletBurst()
	{
		GameManager.init.frequency = 0.02f;
	}

	public void normal() 
	{
		Color color = GetComponent<Renderer>().material.color;
		GetComponent<Renderer>().material.color = new Color(color.r, color.g,color.b,1f);
	}

	public void CoulDown()
	{
		if (shieldCouldown)
		{
			timeA += Time.deltaTime;
			if (timeA >= timeCoulDown)
			{
				timeA = 0;
				shieldCouldown = false;

			}
		}
		//
		if (DashCouldown)
		{
			timeB += Time.deltaTime;
			if (timeB >= timeDash)
			{
				timeB = 0;
				DashCouldown = false;

			}
		}
		//
		if (jumpCouldown)
		{
			timeC += Time.deltaTime;
			if (timeC >= timeJump)
			{
				timeC = 0;
				jumpCouldown = false;

			}
		}
		//
		if (invisibleCouldown)
		{
			timeD += Time.deltaTime;
			if (timeD >= timeinvisible)
			{
				timeD = 0;
				invisibleCouldown = false;

			}
		}
		//
		if (explosionCouldown)
		{
			timeE += Time.deltaTime;
			if (timeE >= timeExplosion)
			{
				timeE = 0;
				explosionCouldown = false;
			}
		}
		//
		if (rechargeCouldown)
		{
			timeF += Time.deltaTime;
			if (timeF >= timeRecharge)
			{
				timeF = 0;
				rechargeCouldown = false;
			}
		}
	}

	public void endShieldPasive()
	{
		pasiveShield = true;
	}

	public void abilitiePasiveShildHP()
	{
		if (GameManager.init.player1Hp <= GameManager.init.player1Hp / 2 && pasiveShield) 
		{
			pasiveShield = false;
			GameObject clon = (GameObject)Instantiate (shieldPasive, transform.position, Quaternion.identity,gameObject.transform);
			clon.transform.localScale = new Vector3 (3f, 3f, 3f);
			Destroy (clon,timeShield);
			Invoke ("endShieldPasive",timeShield+timeShield);
		}
	}

	public void NormalVelocity()
	{
		GameManager.init.speed = GameManager.init.speedNormal;
	}
}
