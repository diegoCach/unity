﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour 
{
	private bool notification;
	public float hp;
	// Use this for initialization
	void Start () 
	{
		if (gameObject.name== "Player1") 
		{
			hp = GameManager.init.player1Hp;
		} 
		else if (gameObject.name == "Player2") 
		{
			hp = GameManager.init.player2Hp;
		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameObject.name == "Player1") 
		{
			GameManager.init.player1Hp=hp;
			gameOver ();
			//si soy el player 1 y player 2 a perdido
			if(GameManager.init.pl2 && notification)
			{
				notification = false;
				GameManager.init.winPlayer1 ();
			}
		} 
		if (gameObject.name == "Player2") 
		{
			GameManager.init.player2Hp=hp;
			gameOver ();
			//si soy el player 2 y player 1 a perdido
			if(GameManager.init.pl1 && notification)
			{
				notification = false;
				GameManager.init.winPlayer2 ();
			}
		}
			
	}

	public void gameOver()
	{
		if (hp <= 0) 
		{
			hp = 1;
			if (GameManager.init.player1Hp < GameManager.init.player2Hp) 
			{
				GameManager.init.pl1 = true;
			} 
			if (GameManager.init.player2Hp < GameManager.init.player1Hp) 
			{
				GameManager.init.pl2 = true;
			}
			GameManager.init.viewGameWinLoser();
		}
	}
}
