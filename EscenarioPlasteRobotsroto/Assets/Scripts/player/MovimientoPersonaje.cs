﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(CharacterController))]
public class MovimientoPersonaje : NetworkBehaviour 
{
	#region FIELDS REGION
	public float speed = 1.5f;
	public float run = 0;
    public float jump = 0;
	public float gravity = 20f;
	private float moveX;
	private float moveZ;
	private float moveS=0;
	private float moveR;
	public GameObject referenceFloor;
	public Rigidbody playerRb;
	public GameObject mainCamera;
	Animator anim;
	public Vector3 moveDirection = Vector3.zero;
	CharacterController character;
	public GameObject cameraPrefab;
	public int num = 0;

	public Transform showInInspect;
	#endregion

	void Start () 
	{
		if(isLocalPlayer){
			mainCamera = Instantiate (cameraPrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
			num = num +1;
			mainCamera.name = "CameraRigPlayer" + num;
			CameraRig.target = transform;
			showInInspect = CameraRig.target;
			//localPlayerAuthority

			playerRb = GetComponent<Rigidbody>();
			anim = GetComponent<Animator>();
			character =GetComponent<CharacterController>();
			jump = GameManager.init.jump;
			gravity=GameManager.init.gravity;
	
			speed = GameManager.init.speed;
			moveX = anim.GetFloat ("BlendZ");//entra en el animator y recoge el float
			moveZ = anim.GetFloat ("BlendX");
		
		}else{
			return;
		}

	}



    void Update () 
	{

		if (isLocalPlayer) {
			GameManager.init.speed = speed;
			//-------------------------------------------------------------------------------------------------------------------------

			//Llamamos a la coorrutinas para ejecutar las animaciones cuando nos movamos:
			StartCoroutine ("Move");
			moveX = 0.0f; //Cambia las variables del Blend Tree que controla las animaciones
			anim.SetFloat ("BlendX", moveX);
			moveZ = 0.0f;
			anim.SetFloat ("BlendZ", moveZ);
			//anim.SetBool("jumping", false);
			//-------------------------------------------------------------------------------------------------------------------------
			//Rotacion con respecto a la camara
			Quaternion CharacterRotation = mainCamera.transform.GetChild (0).transform.GetChild (0).GetComponent<Camera> ().transform.rotation;
			CharacterRotation.x = 0;
			CharacterRotation.z = 0;

			transform.rotation = CharacterRotation;

			Movimiento ();
		} else {
			return;
		}
	}


									//Corrutina para el movimiento:
	//-------------------------------------------------------------------------------------------------------------------------
	/**
	 * Esta coorrutina ejecuta las animaciones 
	 **/
	IEnumerator Move() 
	{
		if (Input.GetKey (KeyCode.W)) 
		{
			for (float i = 0.0f; i <= 5.0f; i += 1f) 
			{
				moveZ += 1;
				anim.SetFloat ("BlendZ", moveZ);
				yield return null;
			}
		}

		if (Input.GetKey (KeyCode.D)) 
		{
			for (float i = 0.0f; i <= 5.0f; i += 1f) 
			{
				moveX += 1;
				anim.SetFloat ("BlendX", moveX);

				yield return null;
			}
		}
		if (Input.GetKey (KeyCode.A)) 
		{
			for (float i = 0.0f; i >= -10.0f; i -= 1f) 
			{
				moveX -= 1;
				anim.SetFloat ("BlendX", moveX);

				yield return null;
			}
		}
		if (Input.GetKey (KeyCode.S)) 
		{
			for (float i = 0.0f; i <= 10.2f; i += 1f) 
			{
				moveZ += 1;
				anim.SetFloat ("BlendZ", moveZ);

				yield return null;
			}
		}

		//Salto:

		if (Input.GetKeyDown(KeyCode.Space))
		{
			//anim.SetBool("jumping", true);
			for (float i = 0.0f; i <= 5.0f; i += 1f) 
			{
				moveS += 1;
				anim.SetFloat ("BlendS", moveS);

				yield return new WaitForSeconds(0.1f);
			}
			anim.SetFloat ("BlendS", -1);
			Debug.Log("salto");
			yield return null;
		}

		//correr

		if (Input.GetKey(KeyCode.LeftShift) && character.isGrounded)
		{
			
			for (float i = 0.0f; i <= 3.0f; i += 1f) 
			{
				moveR = 2;
				anim.SetFloat ("BlendR", moveR);

				yield return new WaitForSeconds(0.1f);
			}
			
			//anim.SetFloat ("BlendR", -2);

			//yield return null;
		}
		if (Input.GetKeyUp (KeyCode.LeftShift) && character.isGrounded) {
			anim.SetFloat ("BlendR", -2);
			moveR = 0;


		}
    }


	//-------------------------------------------------------------------------------------------------------------------------

												//Funcion para el movimiento:
	//-------------------------------------------------------------------------------------------------------------------------
	/**
	 * Esta funcion mueve al personaje (translada su posicion). 
	**/
	void Movimiento()
	{
		//no se puede saltar y moverse pero si se puede moverse y saltar
		if (character.isGrounded) 
		{
			moveDirection=new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
			moveDirection= transform.TransformDirection (moveDirection);//direccion del vector es la direccion de nuetro transform
			moveDirection *= GameManager.init.speed;
		} 

        //Salto:
		if (Input.GetKeyDown(KeyCode.Space) && character.isGrounded)
        {
			moveDirection.y = jump * 1.3f; // nunca puede ir delante la gravedad o no podra saltar
        }
			
		//agacharse:
		if (Input.GetKeyDown(KeyCode.LeftControl) && character.isGrounded)
		{
			playerRb.gameObject.GetComponent<CapsuleCollider>().height=0.70f;
			//animacion
		}
		if (Input.GetKeyUp(KeyCode.LeftControl) && character.isGrounded)
		{
			playerRb.gameObject.GetComponent<CapsuleCollider>().height=0.98f;

		}


		// correr
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			speed=speed + 2;
		}
		if (Input.GetKeyUp(KeyCode.LeftShift))
		{
			speed=speed - 2;
		}

		moveDirection.y -= gravity * Time.deltaTime;//gravedad para que el objeto caiga (anular gravedad del rigidbody o cae hasta el inifinito)
		character.Move (moveDirection*Time.deltaTime);
    }		

}