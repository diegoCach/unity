﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Attack : NetworkBehaviour  
{
	private bool BvsBRigth=true;//si es cuerpo a cuerpo
	private bool BvsBLeft=false;
	private bool shooterI=true;//si dispara
	private bool shooterR=false;
	private bool rechargePass;
	public GameObject prefab;
	public Transform spawnI;
	public Transform spawnR;
	public float forceAtack;
	private float timeA;
	public int bullet;
	public int timeRechange;
	public float atackPistol=0;
	public float atackSword = 0;
	Animator anim;
	// Use this for initialization
	void Start () 
	{
		bullet = GameManager.init.bullet;
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!isLocalPlayer){
			return;
		}

		if (Input.GetKey(KeyCode.Mouse0)&&BvsBLeft) 																
		{
			//animacion
		}
		if (Input.GetKey(KeyCode.Mouse1)&&BvsBRigth) 																
		{
			//animacion
			atackSword += 1;
			Debug.Log("golpe");
			anim.SetFloat ("atackSword", atackSword);
			Invoke ("volveranim2",1.2f);

		}
		if (Input.GetButtonDown("Fire1")&&shooterI && rechargePass==false) 																
		{
			timeA += Time.deltaTime;
			Shooter (spawnI);
			Debug.Log("disparo");
			atackPistol += 1;
			anim.SetFloat ("atackPistol", atackPistol);
			Invoke ("volveranim",1.1f);
			//animacion
		}
		if (Input.GetButtonDown("Fire2")&& shooterR && rechargePass==false) 																
		{
			timeA += Time.deltaTime;
			Shooter (spawnR);
			//animacion
		}

		rechargeBullet ();
		//spawnI.Rotate(mainCameraComponent.transform.rotation.eulerAngles.x,0,0);
	}

	//metodos

	public void Shooter(Transform arm)
	{
		if(timeA>=GameManager.init.frequency)
		{
			GameObject clon = (GameObject)Instantiate (prefab, arm.position, Quaternion.identity);
			clon.GetComponent<Rigidbody> ().AddForce (arm.transform.forward *forceAtack, ForceMode.Impulse);
			timeA = 0;
			bullet--;
			Destroy (clon, 5);
			//animacion
		}
	}

	private void rechargeBullet()
	{
		if(bullet<=0)
		{
			rechargePass = true;
			bullet = 0;
			StartCoroutine (recharge());
		}
	}

	IEnumerator recharge()
	{
		yield return new WaitForSeconds(timeRechange);
		bullet = GameManager.init.bullet;//numero del gameControler
		rechargePass =false;
	}

	private void volveranim ()
	{
		anim.SetFloat ("atackPistol", -1);
	}
	private void volveranim2 ()
	{
		anim.SetFloat ("atackSword", -1);
	}
}
