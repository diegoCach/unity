﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
	public static GameManager init;
	public GameObject winner;
	public GameObject loser;
	public GameObject finallyG;
	public GameObject player1;
	public GameObject player2;
	public float jump = 0;
	public float gravity = 20f;
	public float copyHP1;
	public float copyHP2;
	public float player1Hp;
	public float speed;
	public float speedNormal;
	public float player2Hp;
	public float player3;
	public float damageBullet;
	public float damageArea;
	public float slowDawnVelocity;//relentizar al recibir el daño en area
	public float slowDawntime;//tiempo que dura la disminucion de velocidad del daño en area
	public int bullet;
	public int PasiveDamageBullet;//daño de la ultima bala de la habilidad pasiva
	public float dashImpulse;//inpulso que realiza la habilidad de Dash
	public float frequency = 0.05f;// velocidad de disparo
	public float JumpPasiveAbilitie;
	public float timeShield;
	public float timeDash;
	public float timeJump;
	public float timeinvisible;
	public float timeRecharge;
	public float timeExplosion;
	public float timeGame;
	public float timeContinue;
	public int roundP1;
	public int roundP2;
	public int rounds;
	private Camera mainCamera;
	public GameObject cam;
	public GameObject spawnCam;
	public Image crossfire;
	public bool pl1;
	public bool pl2;
	private bool winRound=false;
	void Awake()
	{
		init = this;
	}

	// Use this for initialization
	void Start () 
	{
		speed = 3f;
		mainCamera = Camera.main;
		speedNormal = speed;
		copyHP1 = player1Hp;
		copyHP2 = player2Hp;
		player1 = GameObject.Find ("Player1");
		player2 = GameObject.Find ("Player2");
	}
	
	// Update is called once per frame
	void Update () 
	{
		player1 = GameObject.Find ("Player1");
		player2 = GameObject.Find ("Player2");
		timeContinue +=Time.deltaTime;
		TimeGame ();
		/*mainCamera = Camera.main;
		if(mainCamera == null){
			Instantiate (cam, spawnCam.transform.position,Quaternion.identity);
		}*/
	}

	public void TimeGame()
	{
		if (timeGame<timeContinue) 
		{
			timeContinue = 0;
			Rounds ();
		}
	}

	public void Rounds()
	{
		if (player1Hp < player2Hp && !winRound) 
			{
				if (rounds < 3) 
				{
					roundP2++;
					// cartel perder
					loser.SetActive (true);
					player1.GetComponent<MovimientoPersonaje> ().enabled = false;
					player1.GetComponent<Attack> ().enabled = false;
					player1.GetComponent<Abilities> ().enabled = false;
				}
				else 
				{
					endTime ();
				}
			winRound = true;
			} 
			if(player1Hp > player2Hp && !winRound)
			{
				winRound = true;
				if (rounds < 3) 
				{
					roundP1++;
					//cartel ganar
					winner.SetActive (true);
					player1.GetComponent<MovimientoPersonaje> ().enabled = false;
					player1.GetComponent<Attack> ().enabled = false;
					player1.GetComponent<Abilities> ().enabled = false;
				} 
				else 
				{
					endTime ();
				}
			}
			rounds++;
	}

	public void endTime()
	{
		
		if (roundP1 > roundP2) 
		{
			//cartel final partida 
			finallyG.SetActive(true);
			//nombre jugador
			player1.GetComponent<MovimientoPersonaje> ().enabled = false;
			player1.GetComponent<Attack> ().enabled = false;
			player1.GetComponent<Abilities> ().enabled = false;
		} 
		else if (roundP1 < roundP2) 
		{
			//final partida
			finallyG.SetActive(true);
			//nombre jugador
			player1.GetComponent<MovimientoPersonaje> ().enabled = false;
			player1.GetComponent<Attack> ().enabled = false;
			player1.GetComponent<Abilities> ().enabled = false;
		}

	}

	public void viewGameWinLoser()
	{
		if(pl1)
		{
			if (rounds < 3) 
			{
				roundP2++;
				// cartel perder
				loser.SetActive (true);
				player1.GetComponent<MovimientoPersonaje> ().enabled = false;
				player1.GetComponent<Attack> ().enabled = false;
				player1.GetComponent<Abilities> ().enabled = false;
			}
			else 
			{
				endTime ();
			}
			rounds++;

		}
		if(pl2)
		{
			if (rounds < 3) 
			{
				roundP1++;
				//cartel ganar
				winner.SetActive (true);
				player1.GetComponent<MovimientoPersonaje> ().enabled = false;
				player1.GetComponent<Attack> ().enabled = false;
				player1.GetComponent<Abilities> ().enabled = false;
			} 
			else 
			{
				endTime ();
			}
			rounds++;

		}
	}

	public void winPlayer2()
	{
		roundP2++;
		// cartel perder
		winner.SetActive (true);
		player1.GetComponent<MovimientoPersonaje> ().enabled = false;
		player1.GetComponent<Attack> ().enabled = false;
		player1.GetComponent<Abilities> ().enabled = false;
		rounds++;
		endTime ();
		pl2 = false;
	}

	public void winPlayer1()
	{
		roundP1++;
		// cartel perder
		winner.SetActive (true);
		player1.GetComponent<MovimientoPersonaje> ().enabled = false;
		player1.GetComponent<Attack> ().enabled = false;
		player1.GetComponent<Abilities> ().enabled = false;
		rounds++;
		endTime ();
		pl1 = false;
	}


}
