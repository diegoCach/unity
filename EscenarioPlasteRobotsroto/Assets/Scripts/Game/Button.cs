﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour {
	public GameObject winner;
	public GameObject loser;
	public GameObject fin;
	public GameObject [] players;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void ButtonReturn()
	{
		players=GameObject.FindGameObjectsWithTag ("Player");

		foreach (GameObject player in players)
		{
			player.GetComponent<MovimientoPersonaje> ().enabled = true;
			player.GetComponent<Attack> ().enabled = true;
			player.GetComponent<Abilities> ().enabled = true;
			player.GetComponent<Life> ().hp = GameManager.init.copyHP1;
			player.GetComponent<Life> ().hp = GameManager.init.copyHP2;
		}
		loser.SetActive(false);
		winner.SetActive(false);
		fin.SetActive(false);
	}
}
